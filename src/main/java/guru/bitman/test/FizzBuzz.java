package guru.bitman.test;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

class FizzBuzz {
    private static Map<Function<Integer,Boolean>, Function<Integer,String>> mapOf = new HashMap<>();

    static {
        mapOf.put(i -> i % 3 == 0 && i % 5 == 0, i -> "FizzBuzz" );
        mapOf.put(i -> i % 3 == 0 && i % 5 != 0, i -> "Fizz" );
        mapOf.put(i -> i % 3 != 0 && i % 5 == 0, i -> "Buzz" );
        mapOf.put(i -> true, String::valueOf);
    }

    List<String> run() {
        return IntStream.rangeClosed(1, 100)
                .mapToObj(this::mapToString)
                .collect(Collectors.toList());
    }

    private String mapToString(int i) {
        for ( Map.Entry<Function<Integer, Boolean>, Function<Integer,String>> entry : mapOf.entrySet() ) {
            if( entry.getKey().apply(i) )
                return entry.getValue().apply(i);
        }

        throw new RuntimeException("DefaultStrategy missing :O");
    }
}
